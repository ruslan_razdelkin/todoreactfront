import React, {Component} from 'react'
import './add-item.css';

export default class AddItem extends Component {

    state = {
        label: ""
    }

    onSubmit = (e) => {
        e.preventDefault()
        this.props.onItemAdded(this.state.label)
    }

    onChange = (e) => {
        this.setState({
            label: e.target.value
        })
    }

    render() {
        const classNames = "add-item input-group mb-3 d-flex"

        return (
            <form className={classNames} onSubmit={this.onSubmit}>
                <input placeholder="Enter todo" className="form-control" onChange={this.onChange}/>
                <button className="btn btn-primary btn-sm float-right input-group-prepend">
                    Add
                </button>
            </form>
        )
    }

}