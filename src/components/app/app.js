import React, {Component} from 'react';

import AppHeader from '../app-header';
import SearchPanel from '../search-panel';
import TodoList from '../todo-list';
import AddItem from '../item-add-form';
import ItemStatusFilter from '../item-status-filter';
import TodoService from '../../services/todo-service';
import './app.css';

export default class App extends Component {

    state = {
        todoData: [],
        term: ''
    };

    onToggleDone = (id) => {
        this.changeParam(id, "done")
    }

    onToggleImportant = (id) => {
        this.changeParam(id, "important")
    }

    changeParam = (id, propName) => {
        return this.setState(({todoData}) => {
            const idx = todoData.findIndex(value => value.id === id)
            const oldItem = todoData[idx]
            const newItem = {
                ...oldItem,
                [propName]: !oldItem[propName]
            }
            const newArr = [
                ...todoData.slice(0, idx),
                newItem,
                ...todoData.slice(idx + 1)]
            return {
                todoData: newArr
            }
        })
    }


    deletedItem = (id) => {
        return this.setState(({todoData}) => {
            const idx = todoData.findIndex(value => value.id === id)
            const newArr = [
                ...todoData.slice(0, idx),
                ...todoData.slice(idx + 1)]
            return {
                todoData: newArr
            }
        })
    }

    addItem = (text) => {
        return {
            label: text,
            important: false,
            done: false,
            id: this.state.todoData.length + 1
        }
    }

    onItemAdded = (text) => {
        return this.setState(({todoData}) => {
            const newRow = this.addItem(text)
            const newArr = [...todoData, newRow]
            return {
                todoData: newArr
            }
        })
    }

    searchItem = (todoData, term) => {
        return todoData.filter(value => value.label.includes(term))
    }

    onSearch = (term) => {
        return this.setState(() => {
            return {
                term: term
            }
        })
    }

    componentDidMount() {
        new TodoService()
            .getAllTodo()
            .then((res) => {
                    this.setState({
                        todoData: res
                    })
                }
            )
    }

    render() {
        const {todoData, term} = this.state
        const visibleItems = this.searchItem(todoData, term);
        const todoCount = this.state.todoData.length
        const doneCount = this.state.todoData.filter(value => value.done).length
        return (
            <div className="todo-app">
                <AppHeader toDo={todoCount} done={doneCount}/>
                <div className="top-panel d-flex">
                    <SearchPanel onSearch={this.onSearch}/>
                    <ItemStatusFilter/>
                </div>
                <TodoList
                    todos={visibleItems}
                    onToggleImportant={this.onToggleImportant}
                    onToggleDone={this.onToggleDone}
                    onDeleted={this.deletedItem}
                />
                <AddItem
                    onItemAdded={this.onItemAdded}/>
            </div>
        );
    }
};

